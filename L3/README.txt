Participants : Deepak Yadav  		User ID:dy329
	      

In this lab we used Android studio to create a tic tac toe game. I used 9 buttons in 3X3 grid and one restart(start game again) button as well as one 
to display result whether its a tie or a player win.

1.I added onclick listner to all 9 buttons and one seperate listerner to start game again button.

2.I created game.java class to declare my game variables and functions for my layout and game board.

3.I used game board array to store status of game and boolean turn variable to track the turn of user.and after every click I will change the 
turn and increase turn count also based on turn I will set text to buttons and add integer data to our board array.

4.Also after changing text I will check if there is a winner and also check for draw and to make sure that user doesnt click the button 
again I set its setclickable proerty to false.If all buttons are clicked and there is no winner than it will display draw as result.

5.Whenever game is finished(draw or a player wins) we can click start again to restart game and and it will make all buttons clickable and 
initialize game board array and turn variables as well as turncount to 0.

6.I made function to check for winner which check for winning condition i.e. 3 horizontal symbol are same or 3 vertical or 2 diagonal are same.

7.even if a user won before all 9 turns and game is complete I will make all button unclickable untill we restart the game.

Testing: I checked the game for all possible Cases like X wins O wins or draw as well as all type of winning cases.
 
A screencast video is also saved in L3 folder.