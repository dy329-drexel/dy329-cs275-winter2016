package com.example.deepak.tic_tac;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.Toast;

import static com.example.deepak.tic_tac.game.buttonClicked;
import static com.example.deepak.tic_tac.game.initGame;


public class MainActivity extends Activity implements android.view.View.OnClickListener{




    Button b1,b2,b3,b4,b5,b6,b7,b8,b9;
    Button[] array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Listeners for buttons
b1=(Button)findViewById(R.id.B1);
        b1=(Button)findViewById(R.id.B1);
        b2=(Button)findViewById(R.id.B2);
        b3=(Button)findViewById(R.id.B3);
        b4=(Button)findViewById(R.id.B4);
        b5=(Button)findViewById(R.id.B5);
        b6=(Button)findViewById(R.id.B6);
        b7=(Button)findViewById(R.id.B7);
        b8=(Button)findViewById(R.id.B8);
        b9=(Button)findViewById(R.id.B9);
        Button reset=(Button)findViewById(R.id.Reset);
       final Button text1=(Button)findViewById(R.id.button);

        array=new Button[]{b1,b2,b3,b4,b5,b6,b7,b8,b9};
//Seperate listener for restart button
reset.setOnClickListener(new OnClickListener() {
    @Override
    public void onClick(View v) {
        initGame(array,text1);
//initialize the game again
    }
});

      for(Button b:array)
      {
          b.setOnClickListener( this);
      }
    }
    @Override
    public void onClick(View v){
Button b=(Button) v;
Button text1=(Button)findViewById(R.id.button);
       buttonClicked(b,text1,array);
//calling function buttonclicked from class game.java
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
