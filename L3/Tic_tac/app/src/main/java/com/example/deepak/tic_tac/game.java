package com.example.deepak.tic_tac;

import android.view.Display;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Deepak on 2/23/2016.
 */
public class game {

//class variables and board array declaration
    public static final int EMPTY = 0;
    public static final int CROSS = 1;
    public static final int NOUGHT = 2;

    public static int[] board = new int[9];

    static boolean turn=true;//x is true o is false
   static int turn_count=0;

    //initialize the game
    public static void initGame(Button a[],Button dres) {
        for (int i = 0; i< 9; i++) {
           board[i]=0;
        }
        turn_count=0;
       turn=true; // ready to play
            dres.setText("Result");

        for(Button b:a)
        {
            b.setClickable(true);

            b.setText("");}
    }

//when a button is clicked change the game status and buttons text
    static void buttonClicked(Button b,Button dres,Button a[]) {

        if (turn) {
            // cross plays first
            b.setText("X");
            add(b, CROSS);
        } else {

            b.setText("O");
            add(b, NOUGHT);

        }

        b.setClickable(false);
        turn_count++;
        draw(dres);
        if (winner(dres,b)) {
            for (Button all : a) {
                all.setClickable(false);
            }
        }
        turn = !turn;   //advance to next players turn
    }

    //to checkif there is a winner
public static boolean winner(Button dres,Button  b){
    int flag=0;
    if (board[0]==board[1]&&board[1]==board[2]&&board[0]!=0)
        flag=1;
    else if (board[3]==board[4]&&board[4]==board[5]&&!b.isClickable()&&board[4]!=0)
        flag=1;
    else if (board[6]==board[7]&&board[7]==board[8]&&!b.isClickable()&&board[6]!=0)
        flag=1;

    else if (board[0]==board[3]&&board[3]==board[6]&&!b.isClickable()&&board[0]!=0)
        flag=1;
    else if (board[1]==board[4]&&board[4]==board[7]&&!b.isClickable()&&board[4]!=0)
        flag=1;
    else if (board[2]==board[5]&&board[5]==board[8]&&!b.isClickable()&&board[2]!=0)
        flag=1;

    else if (board[0]==board[4]&&board[4]==board[8]&&!b.isClickable()&&board[0]!=0)
        flag=1;
    else if (board[2]==board[4]&&board[4]==board[6]&&!b.isClickable()&&board[2]!=0)
        flag=1;


    if(flag==1&& turn)
    {  dres.setText("X won");  return true;}
    if(flag==1&& !turn)
    {  dres.setText("O won"); return true; }
return false;
}

//add values to game array 0 is blank 1 is cross 2 is for O
    public static void  add(Button b,int a)
    { int abc=b.getId();
        if(abc==R.id.B1) {
            board[0] = a;
        }
        if(abc==R.id.B2) {
            board[1] = a;
        }
        if(abc==R.id.B3) {
            board[2] = a;
        }
        if(abc==R.id.B4) {
            board[3] = a;
        }
        if(abc==R.id.B5) {
            board[4] =a;
        }
        if(abc==R.id.B6) {
            board[5] = a;
        }
        if(abc==R.id.B7) {
            board[6] = a;
        }
        if(abc==R.id.B8) {
            board[7] = a;
        }
        if(abc==R.id.B9) {
            board[8] = a;

        }

    }
    //check if there are all nine button clicked and no winner
    public static void draw(Button a) {
if(turn_count==9)
a.setText("draw");
    }

}

