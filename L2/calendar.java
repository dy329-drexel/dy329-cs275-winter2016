package calender;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class calendar {
	
	public static void main(String args[])throws Exception {
	// Declaring client id and credentials 
	 String cId = "970064356252-4pb6if7usoeteonp8h9tkrkhh5eg39rq.apps.googleusercontent.com";
     String cSecret = "6uJ9OgqyZAzMDtMfhCJe2ap2";
     String rUri = "http://www.drexel.edu";
     
     //Url for asking permissions of user account
     String  Url= "https://accounts.google.com/o/oauth2/auth?"+ "access_type=offline"+ "&client_id=" + cId+ "&scope=https://www.googleapis.com/auth/calendar"+ "&response_type=code"+ "&redirect_uri=" + rUri + "&state=/profile"+ "&approval_prompt=force";
     
     //Getting the access code from the user
     System.out.println("Copy and paste this address in your web browser" +Url);
     System.out.println("type  acces code you got");
     System.out.println(":");
     Scanner in = new Scanner(System.in);
     String Accesscode = in.nextLine();
     
     //Executing a post command to get access token
     //executePost method taken from professor mongan's slides.
     String Url1 = "https://accounts.google.com/o/oauth2/token";
     String details = "code=" + Accesscode + 
				"&client_id=" + cId + 
				"&client_secret=" + cSecret +
				"&redirect_uri=" + rUri + 
				"&grant_type=authorization_code";
     in.close();
     String PResponse = executePost(Url1,details);
     
     //Getting access token from post request
     
     JsonParser jp = new JsonParser();
     JsonElement root = jp.parse(PResponse);
     JsonObject rootobj = root.getAsJsonObject();
     String token = rootobj.get("access_token").getAsString();
   
     // new get request for calendar events
     String listUrl = "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token="+token;
     URL url=new URL(listUrl);;
     HttpURLConnection request = (HttpURLConnection) url.openConnection();;
       
         request.connect();        
         JsonElement root1 = jp.parse(new InputStreamReader((InputStream) request.getContent()));
         JsonObject rootObj = root1.getAsJsonObject();
         
         //Parsing calender list
         JsonArray list = rootObj.get("items").getAsJsonArray();
         
       
         //selecting any first calendar and making new request
         
         String calendar = list.get(0).getAsJsonObject().get("id").getAsString();
         String events = "https://www.googleapis.com/calendar/v3/calendars/"+calendar+"/events?access_token="+token;
         url = new URL(events);
         System.out.println(events);
         request = (HttpURLConnection) url.openConnection();
      
         
         JsonElement root2 = jp.parse(new InputStreamReader((InputStream) request.getContent()));
         rootObj = root2.getAsJsonObject();
         
         //parsing array of items and displaying id summary and time
         
         JsonArray event = rootObj.get("items").getAsJsonArray();
      System.out.println("all events from first calender");
         for(int i=0; i<event.size(); i++){
             String summery = event.get(i).getAsJsonObject().get("summary").getAsString();
             String Id=event.get(i).getAsJsonObject().get("id").getAsString();
             String dateTime = event.get(i).getAsJsonObject().get("start").getAsJsonObject().get("dateTime").getAsString();
             System.out.println("id:"+Id);
             System.out.println(summery+ "\n " + dateTime);
         }
     
     
 }
 
// execute method from professor's slides
 public static String executePost(String targetURL, String urlParameters) {
		URL url;
		HttpURLConnection connection = null;  
     try {
     	// Create connection
     	url = new URL(targetURL);
     	connection = (HttpURLConnection)url.openConnection();
     	connection.setRequestMethod("POST");
     	connection.setRequestProperty("Content-Type", 
           "application/x-www-form-urlencoded");
         
     	connection.setRequestProperty("Content-Length", "" + 
     			Integer.toString(urlParameters.getBytes().length));
     	connection.setRequestProperty("Content-Language", "en-US");  
         
     	connection.setUseCaches (false);
     	connection.setDoInput(true);
     	connection.setDoOutput(true);
      
     	// write parameters to request
     	DataOutputStream wr = new DataOutputStream (
                  connection.getOutputStream ());
     	wr.writeBytes (urlParameters);
     	wr.flush ();
     	wr.close ();

     	// read response from request 
     	InputStream is = connection.getInputStream();
     	BufferedReader rd = new BufferedReader(new InputStreamReader(is));
     	String line;
     	StringBuffer response = new StringBuffer(); 
      
     	while((line = rd.readLine()) != null) {
     		response.append(line);
     		response.append('\r');
     	}
     	rd.close();
      
     	return response.toString();
     } catch (Exception e) {
     	
     	e.printStackTrace();
         return null;
     } finally {
     	if(connection != null) {
     		connection.disconnect(); 
     	}
     }
 }
 }
 
