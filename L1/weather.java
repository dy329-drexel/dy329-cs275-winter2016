//package wi;


		import java.net.*;
		import java.io.*;
		import com.google.gson.*;

		public class weather{
		public static void main (String[] args) throws Exception{
			
			//making connection for given url
			String url="http://api.wunderground.com/api/25dbf314891964bc/geolookup/q/autoip.json";
			URL request= new URL(url);
			URLConnection connection=request.openConnection();
			
            		//Gson pareser to parse json response from API
			JsonParser gson = new JsonParser();
			JsonElement root = gson.parse(new InputStreamReader((InputStream) connection.getContent()));
			JsonObject rootobj = root.getAsJsonObject();
			
			// Using user's Ip address for finding state,zipcode and city name and parse data from Json object
			String zipcode=rootobj.get("location").getAsJsonObject().get("zip").getAsString();
			String city=rootobj.get("location").getAsJsonObject().get("city").getAsString();
			String state=rootobj.get("location").getAsJsonObject().get("state").getAsString();
		        System.out.println("User's Zipcode:"+zipcode);
		        System.out.println("User's City:"+city);
		        System.out.println("User's State:"+state);
		        System.out.println("-----------------------------------------------------");
		
		       //Use user's city and state to find hourly forecast
		       String url1="http://api.wunderground.com/api/25dbf314891964bc/hourly/q/"+state+"/"+city+".json";
		     
		       URL forcast= new URL(url1);
		       URLConnection connection1=forcast.openConnection();
		       JsonParser gson1 = new JsonParser();
		       JsonElement root1 = gson1.parse(new InputStreamReader((InputStream) connection1.getContent()));
		       JsonObject rootobj1 = root1.getAsJsonObject();
		
	   	       JsonArray ar = rootobj1.get("hourly_forecast").getAsJsonArray();
		    
		      //Iterate array to print forecast of every hour
		       for(int i=0;i<ar.size();i++){
			     JsonObject data=ar.get(i).getAsJsonObject();
		             String date=data.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
	
	          	     String temp=data.get("temp").getAsJsonObject().get("english").getAsString();
		             String condition=data.get("condition").getAsString();
		             String relatedh=data.get("humidity").getAsString();

		             System.out.println("date and time: "+date);
			     System.out.println("Temp. in english:"+temp);
		             System.out.println("Current condition:"+condition);
		             System.out.println("humidity :"+relatedh);
		             System.out.println("------------------------------------------");
		    							}

								}
				}
//end of program
