package com.example.deepak.myapplication;

import android.content.Context;

import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;



import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Deepak on 3/3/2016.
 */






    public class weather extends AsyncTask<Void,Void,Void>{

    ArrayList<weatherdetails> data1;
    //public Context context;
    weatherAdapter adapter;
//double lat;
   // double longi;
    public ArrayList<weatherdetails> getdetails(){
        return data1;
    }
 public weather(ArrayList<weatherdetails> data2,weatherAdapter adap){
  data1=data2;
     adapter=adap;
   //  lat=la;
    // longi=lo;

 }

    @Override
    public Void doInBackground(Void... params) {
        try {
           // making connection for given url
            String url = "http://api.wunderground.com/api/25dbf314891964bc/geolookup/q/autoip.json";
           URL request = new URL(url);
           HttpURLConnection connection = (HttpURLConnection) request.openConnection();
           connection.connect();
            //Gson pareser to parse json response from API

           JsonParser gson = new JsonParser();
           JsonElement root = gson.parse(new InputStreamReader((InputStream) connection.getContent()));
           JsonObject rootobj = root.getAsJsonObject();

            // Using user's Ip address for finding state,zipcode and city name and parse data from Json object
         //   String zipcode = rootobj.get("location").getAsJsonObject().get("zip").getAsString();
           String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
            String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();


            //Use user's city and state to find hourly forecast
          String url1 = "http://api.wunderground.com/api/25dbf314891964bc/hourly/q/" + state + "/" + city + ".json";
          //  String url1 = "http://api.wunderground.com/api/25dbf314891964bc/hourly/q/"+lat+","+longi+".json";

            URL lookupweather = new URL(url1);
            HttpURLConnection request1 = (HttpURLConnection)lookupweather.openConnection();
            request1.connect();
            JsonParser gson1 = new JsonParser();
            JsonElement root1 = gson1.parse(new InputStreamReader((InputStream)request1.getContent()));
            JsonObject rootobj1 = root1.getAsJsonObject();

            JsonArray ar = rootobj1.get("hourly_forecast").getAsJsonArray();

            //Iterate array to print forecast of every hour
            for (int i = 0; i <ar.size(); i++) {
                JsonObject data = ar.get(i).getAsJsonObject();
                String date = data.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();

                String temp = data.get("temp").getAsJsonObject().get("english").getAsString();
                String condition = data.get("condition").getAsString();
                String relatedh = data.get("humidity").getAsString();
                String iconurl=data.get("icon_url").getAsString();
                weatherdetails abc;
                abc = new weatherdetails(temp, date, condition, relatedh,iconurl);
                data1.add(abc);

            }


        }  catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }




    @Override
    protected void onPostExecute(Void param)
    {
        adapter.notifyDataSetChanged();
    }

}
