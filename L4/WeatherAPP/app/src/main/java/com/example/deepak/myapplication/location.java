package com.example.deepak.myapplication;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Deepak on 3/5/2016.
 */
public class location extends Service implements LocationListener {
private final Context context;

    double latitude;
    double longitude;
    protected LocationManager location;
    public location(Context context){
        this.context=context;

    }
    public location getLocation(){
        Criteria criteria = new Criteria();


        location=(LocationManager)context.getSystemService(LOCATION_SERVICE);
        String provider = location.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return null;
        }
        Location loc=location.getLastKnownLocation(provider);
        latitude=loc.getLatitude();
        longitude=loc.getLongitude();
        return this;
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
