package com.example.deepak.myapplication;

/**
 * Created by Deepak on 3/5/2016.
 */

import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.os.AsyncTask;
        import android.util.Log;
        import android.widget.ImageView;

        import java.io.InputStream;

public class imageicon extends AsyncTask<String, Void, Bitmap> {
    ImageView Image;

    public imageicon(ImageView bmImage) {
        this.Image = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap Icon = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            Icon = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error in image", e.getMessage());
        }
        return Icon;
    }

    protected void onPostExecute(Bitmap result) {
        Image.setImageBitmap(result);
    }
}