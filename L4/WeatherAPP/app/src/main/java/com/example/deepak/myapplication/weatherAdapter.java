package com.example.deepak.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Deepak on 3/3/2016.
 */
public class weatherAdapter extends ArrayAdapter<weatherdetails> {
    public weatherAdapter(Context context, ArrayList<weatherdetails> details) {
        super(context, 0, details);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

        weatherdetails det1 = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
        }
        // Lookup view for data population
        TextView temp1 = (TextView) convertView.findViewById(R.id.tempView);
        TextView condition1 = (TextView) convertView.findViewById(R.id.descView);
        TextView date1 = (TextView) convertView.findViewById(R.id.dateView);
        TextView humid1 = (TextView) convertView.findViewById(R.id.humidView);
       ImageView abc=(ImageView)convertView.findViewById(R.id.imageView);


       imageicon pic=new imageicon(abc);
pic.execute(det1.iconurl);




      //   Populate the data into the template view using the data object
        temp1.setText(det1.temp);
        condition1.setText(det1.condition);
        date1.setText(det1.dtime);
        humid1.setText(det1.humid);
       // abc.setImageBitmap(det1.bmp);
        // Return the completed view to render on screen
        return convertView;
    }
}
