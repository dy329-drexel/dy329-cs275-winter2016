package com.example.deepak.stash1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.deepak.stash1.Stash.Item;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.temboo.Library.eBay.Finding.FindItemsAdvanced;
import com.temboo.Library.eBay.Finding.FindItemsAdvanced.FindItemsAdvancedInputSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.io.IOException;
import java.io.InputStream;

import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Kate Arskaya and Deepak on 3/11/16.
 */
public class EbayListTask extends AsyncTask<Void, Void, Void> {
    ArrayList<Item> data;
    EbayAdapter adapter;
    Context context;
    String keyword;


public Item getselecteditem(int pos){
    return data.get(pos);

}

    //ebay app id
    public static String APPID = "DrexelUn-StashMon-SBX-e38c4f481-6c9b330a";


    public EbayListTask(ArrayList<Item> data, EbayAdapter adapter, Context context, String keyword) {
        this.data = data;
        this.adapter = adapter;
        this.context = context;
        this.keyword = keyword;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            TembooSession session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");

            FindItemsAdvanced findItemsAdvancedChoreo = new FindItemsAdvanced(session);

// Get an InputSet object for the choreo
            FindItemsAdvancedInputSet findItemsAdvancedInputs = findItemsAdvancedChoreo.newInputSet();

// Set inputs
            findItemsAdvancedInputs.set_Keywords(keyword);
            findItemsAdvancedInputs.set_AppID(APPID);
            findItemsAdvancedInputs.set_SandboxMode("1");
            findItemsAdvancedInputs.set_EntriesPerPage("15");

// Execute Choreo
            FindItemsAdvanced.FindItemsAdvancedResultSet findItemsAdvancedResults = findItemsAdvancedChoreo.execute(findItemsAdvancedInputs);



            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(findItemsAdvancedResults.get_Response());

            JsonObject rootobj = root.getAsJsonObject().get("searchResult").getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

            JsonArray items = rootobj.get("item").getAsJsonArray();
            for(int i = 0; i < items.size(); i++) {

                Item myItem = new Item();
                JsonObject item = items.get(i).getAsJsonObject();

                String title = item.get("title").getAsString();
                myItem.setName(title);

                BigDecimal price = item.get("sellingStatus").getAsJsonObject().get("convertedCurrentPrice").getAsJsonObject().get("#text").getAsBigDecimal();
                myItem.setStashAmount(price);

                String URL = item.get("viewItemURL").getAsString();
                myItem.setUrl(URL);

                try {
                    String picUrl = item.get("galleryURL").getAsString();
                    Bitmap pic = BitmapFactory.decodeStream((InputStream) new URL(picUrl).getContent());
                    myItem.setItem_image(pic);
                } catch (MalformedURLException e) {
                   // Bitmap pic = BitmapFactory.decodeFile(String.valueOf(R.drawable.no_image));
                    Bitmap pic = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_image);
                    myItem.setItem_image(pic);
                } catch (IOException e) {
                    //Bitmap pic = BitmapFactory.decodeFile(String.valueOf(R.drawable.no_image));
                    Bitmap pic = BitmapFactory.decodeResource(context.getResources(),R.drawable.no_image);
                    myItem.setItem_image(pic);
                } catch (Exception e) {
                    Bitmap pic = BitmapFactory.decodeResource(context.getResources(),R.drawable.no_image);
                    myItem.setItem_image(pic);
                }



                data.add(myItem);
            }
        } catch (TembooException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void param) {
        adapter.notifyDataSetChanged();
    }

}
