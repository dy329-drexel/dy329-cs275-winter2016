package com.example.deepak.stash1.Stash;

import java.io.Serializable;
import java.math.BigDecimal;

public class StashFlow implements Serializable
{
	private BigDecimal cflow;
	private BigDecimal percent;

	@Override
	public StashFlow clone() throws CloneNotSupportedException
	{
		return (StashFlow)super.clone();
	}
	
	public BigDecimal getCflow() 
	{
		return cflow;
	}
	
	public void setCflow(BigDecimal cflow) 
	{
		this.cflow = cflow;
	}
	
	public BigDecimal getPercent() 
	{
		return percent;
	}
	
	public void setPercent(BigDecimal percent) 
	{
		this.percent = percent;
	}
	
	public BigDecimal calculateStashFlow(BigDecimal c, BigDecimal s)
	{
	 return cflow.multiply(s.divide(new BigDecimal(100)));
	}
}
