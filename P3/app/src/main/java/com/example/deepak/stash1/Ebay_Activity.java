package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.deepak.stash1.Stash.Item;
import com.example.deepak.stash1.Stash.StashPlan;

import java.util.ArrayList;

/** Created by Kate Arskaya on 03.09.2016 */

public class Ebay_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay);

        final EditText searchbox = (EditText) findViewById(R.id.search_text_ebay);
        Button go_button = (Button) findViewById(R.id.search_button_ebay);
        Button select_button = (Button) findViewById(R.id.select_button_ebay);
        final Button back_button = (Button) findViewById(R.id.back_button_ebay);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(), "fonts/DKAstromonkey.otf");
        go_button.setTypeface(astromonkey_font);
        select_button.setTypeface(astromonkey_font);
        back_button.setTypeface(astromonkey_font);

        final int flag = 0;
        final int pos;

        go_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String search = searchbox.getText().toString();

                ArrayList<Item> items = new ArrayList<Item>();

                EbayAdapter adapter = new EbayAdapter(Ebay_Activity.this, items);
                final EbayListTask ebayTask = new EbayListTask(items, adapter, Ebay_Activity.this, search);

                ListView l = (ListView) findViewById(R.id.listView);
                l.setAdapter(adapter);


                ebayTask.execute();

                ListView abc = (ListView) findViewById(R.id.listView);
                abc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        Item itemselect = ebayTask.getselecteditem(position);
                        startIntent(itemselect);
                        //http://stackoverflow.com/questions/2736389/how-to-pass-an-object-from-one-activity-to-another-on-android


                    }
                });

            }
        });


        back_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Ebay_Activity.this, New_Plan_Spend_Activity.class);
                startActivity(otherIntent);
            }
        });


    }
    public void startIntent(Item itemselect){
        StashPlan plan = new StashPlan();
        String a = "position" + itemselect.getlongprice();
        Item newitem = new Item();

        newitem.setName(itemselect.getName());

        newitem.setStashAmount(itemselect.getlongprice());
        //  bdPrice = new java.math.BigDecimal(String.valueOf(price));
        //    item.setStashAmount(bdPrice);

        plan.setItem(newitem);
      //  Toast.makeText(Ebay_Activity.this, a, Toast.LENGTH_SHORT).show();
        Intent othernewIntent = new Intent(Ebay_Activity.this, Item_Save_Activity.class);
       othernewIntent.putExtra("StashPlan", plan);
        startActivity(othernewIntent);
    }
}
