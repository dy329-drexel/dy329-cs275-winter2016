package com.example.deepak.stash1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deepak.stash1.Stash.Item;

import java.util.ArrayList;

/**
 * Created by Kate Arskaya on 3/11/16.
 */
public class EbayAdapter extends ArrayAdapter<Item> {

    public EbayAdapter(Context context, ArrayList<Item> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Item current_item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_layout_ebay, parent, false);
        }

        ImageView pic = (ImageView) convertView.findViewById(R.id.item_pic_p);
        TextView item_name = (TextView) convertView.findViewById(R.id.item_name_ebay);
        TextView item_url = (TextView) convertView.findViewById(R.id.item_url_ebay);
        TextView price = (TextView) convertView.findViewById(R.id.item_price_ebay);

        pic.setImageBitmap(current_item.getItem_image());
        item_name.setText(current_item.getName());
        item_url.setText(current_item.getUrl());
        price.setText(current_item.getNicePrice());
        return convertView;
    }
}
