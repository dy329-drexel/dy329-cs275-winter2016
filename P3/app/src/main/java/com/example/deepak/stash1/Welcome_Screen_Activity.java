package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Welcome_Screen_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        Button start = (Button)findViewById(R.id.start_button);
        Button login = (Button) findViewById(R.id.login_welcome);
        ImageView image = (ImageView)findViewById(R.id.imageView);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");
        start.setTypeface(astromonkey_font);

        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Welcome_Screen_Activity.this,Registration_Activity.class);
                startActivity(otherIntent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Welcome_Screen_Activity.this,Login_Activity.class);
                startActivity(otherIntent);
            }
        });



    }

//    public void onClick() {
//        Intent otherIntent = new Intent(this, New_Plan_Activity.class);
//        startActivity(otherIntent);
//    }

}
