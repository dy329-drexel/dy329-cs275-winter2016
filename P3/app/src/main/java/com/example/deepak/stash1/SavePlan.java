package com.example.deepak.stash1;

import android.os.AsyncTask;

import com.example.deepak.stash1.Stash.StashPlan;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet.ObjectSetResultSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Deepak on 3/12/2016.
 */
public class SavePlan extends AsyncTask<Void,Void,Void> {
    static final String api_key="66464d573a984729bb40f3167451e437";
    static final String  app_id="8d8687be87ced94d634a2f8b319aa2f7";
    static String session_token;
    JSONObject plandetails=new JSONObject();
    public SavePlan(String stoken,StashPlan newp){
        session_token=stoken;
//add information to json about stashplan
        try {
            plandetails.put("itemName",newp.getItem().getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    @Override
    protected Void doInBackground(Void... params) {

        TembooSession session = null;
        try {
            session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");
        } catch (TembooException e) {
            e.printStackTrace();
        }

        ObjectSet objectSetChoreo = new ObjectSet(session);

    // Get an InputSet object for the choreo
    ObjectSet.ObjectSetInputSet objectSetInputs = objectSetChoreo.newInputSet();

// Set inputs
    objectSetInputs.set_APIKey(api_key);
    objectSetInputs.set_SessionToken(session_token);
    objectSetInputs.set_Data(plandetails.toString());
    objectSetInputs.set_ApplicationIdentifier(app_id);

    // Execute Choreo
        try {
            ObjectSetResultSet objectSetResults = objectSetChoreo.execute(objectSetInputs);
        } catch (TembooException e) {
            e.printStackTrace();
        }


        return null;
    }

}

