package com.example.deepak.stash1.Stash;

import java.io.Serializable;

/**
 * Created by DickandBalls on 3/12/16.
 */
public class User implements Serializable {

    String email;
    String password;
    StashPlan plan;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public StashPlan getPlan() {
        return plan;
    }

    public void setPlan(StashPlan plan) {
        this.plan = plan;
    }
}
