package com.example.deepak.stash1.Stash;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.math.BigDecimal;

public class Item implements Serializable
{
	private String name;
	private String type;
	private BigDecimal stashAmount;
	private String url;
	private Bitmap item_image;

	@Override
	public Item clone() throws CloneNotSupportedException
	{
		return (Item)super.clone();
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public BigDecimal getStashAmount() 
	{
		return stashAmount;
	}
	
	public void setStashAmount(BigDecimal stashAmount) 
	{
		this.stashAmount = stashAmount;
	}
	
	public String getUrl() 
	{
		return url;
	}
	
	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getType() 
	{
		return type;
	}

	public void setType(String type) 
	{
		this.type = type;
	}

	public Bitmap getItem_image() {
		return item_image;
	}

	public void setItem_image(Bitmap item_image) {
		this.item_image = item_image;
	}

	public String getNicePrice() {
		return "$" + stashAmount.toString();
	}
	public BigDecimal getlongprice(){
		return stashAmount;
	}
}