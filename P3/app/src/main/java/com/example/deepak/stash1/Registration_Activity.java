package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by Kate Arskaya on 3/4/16.
 */
public class Registration_Activity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        TextView signup_text = (TextView) findViewById(R.id.signup_text);
        TextView or = (TextView) findViewById(R.id.or_textView);

        Button signup_butt = (Button) findViewById(R.id.sign_up_button);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");

        signup_text.setTypeface(astromonkey_font);
        signup_butt.setTypeface(astromonkey_font);
        or.setTypeface(astromonkey_font);

        final EditText email = (EditText) findViewById(R.id.email_box);

        final EditText pw1 = (EditText) findViewById(R.id.psw1_box);

        final EditText pw2 = (EditText) findViewById(R.id.psw2_box);


        signup_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String api_key = "66464d573a984729bb40f3167451e437";
                String app_id="8d8687be87ced94d634a2f8b319aa2f7";

                String email1=email.getText().toString();
                String pass1=pw1.getText().toString();

                Register task=new Register(email1,pass1,api_key,app_id);
                task.execute();
                Toast.makeText(Registration_Activity.this, "completed", Toast.LENGTH_SHORT).show();
                Intent otherIntent = new Intent(Registration_Activity.this,New_Plan_Activity.class);
                startActivity(otherIntent);
            }
        });


    }



}
