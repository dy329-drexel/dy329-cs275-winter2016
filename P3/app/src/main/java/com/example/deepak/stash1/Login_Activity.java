package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


/**
 * Created by Deepak on 3/4/16.
 */
public class Login_Activity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        final Button signin_button = (Button) findViewById(R.id.sign_in_button);
        TextView label = (TextView) findViewById(R.id.signup_text);
        TextView back = (TextView) findViewById(R.id.back_arrow);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");

        signin_button.setTypeface(astromonkey_font);
        label.setTypeface(astromonkey_font);
        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText email = (EditText) findViewById(R.id.email_box_signin);

                final EditText pw1 = (EditText) findViewById(R.id.psw_box_signin);
                final String uid = email.getText().toString();
                final String pass = pw1.getText().toString();
                //  Toast.makeText(Login_Activity.this, uid, Toast.LENGTH_SHORT).show();


                Login auth = new Login();
                auth.execute(uid,pass);
               // String sst = auth.getSessionToken();
               // Toast.makeText(Login_Activity.this, sst, Toast.LENGTH_SHORT).show();


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Login_Activity.this, New_Plan_Activity.class);
                startActivity(otherIntent);
            }
        });

    }
    protected void startdisplay(String a){
        String sess=a;

        if(sess=="") {
            Toast.makeText(Login_Activity.this,"failed" , Toast.LENGTH_SHORT).show();
        }
        else {
            Log.d("stoken", sess);
            Intent otherIntent = new Intent(Login_Activity.this, New_Plan_Activity.class);
            otherIntent.putExtra("session_token", sess);
            startActivity(otherIntent);
        }
    }


    public class Login extends AsyncTask<String,Void,String> {

        String api_key = "66464d573a984729bb40f3167451e437";
        String app_id = "8d8687be87ced94d634a2f8b319aa2f7";
        String sess="";
        @Override
        protected String doInBackground(String... params) {
            String email=params[0];
            String pass=params[1];

            TembooSession session = null;
            try {
                session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");
            } catch (TembooException e) {
                e.printStackTrace();
            }

            AccountLogin accountLoginChoreo = new AccountLogin(session);

// Get an InputSet object for the choreo
            AccountLogin.AccountLoginInputSet accountLoginInputs = accountLoginChoreo.newInputSet();

// Set inputs
            accountLoginInputs.set_APIKey(api_key);
            accountLoginInputs.set_Username(email);
            accountLoginInputs.set_ApplicationIdentifier(app_id);
            accountLoginInputs.set_Password(pass);

// Execute Choreo
            try {
                AccountLogin.AccountLoginResultSet accountLoginResults = accountLoginChoreo.execute(accountLoginInputs);
                //    sessiont= accountLoginResults.getResultString("session_token");

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(accountLoginResults.get_Response());
                JsonObject rootobj=root.getAsJsonObject();

                sess=rootobj.get("session_token").getAsString();


            } catch (TembooException e) {
                e.printStackTrace();
            }

            return sess;
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            startdisplay(result);

        }
    }


}
