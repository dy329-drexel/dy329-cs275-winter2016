package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.deepak.stash1.Stash.Item;
import com.example.deepak.stash1.Stash.StashPlan;


/**
 * Created by Kate Arskaya on 3/4/16.
 */
public class Item_Save_Activity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_save);

        //just for fonts
        TextView label1 = (TextView) findViewById(R.id.textView);
        TextView label2 = (TextView) findViewById(R.id.label_item);
        TextView label3 = (TextView) findViewById(R.id.label_price);
        TextView label5 = (TextView) findViewById(R.id.label_ok);

        TextView name = (TextView) findViewById(R.id.item_name_save);
        TextView price = (TextView) findViewById(R.id.item_price_save);

     //   TextView url = (TextView) findViewById(R.id.item_url);


        Button confirm = (Button) findViewById(R.id.confirm_item);
        Button back = (Button) findViewById(R.id.back_button_item_save);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(), "fonts/DKAstromonkey.otf");

        label1.setTypeface(astromonkey_font);
        label2.setTypeface(astromonkey_font);
        label3.setTypeface(astromonkey_font);
        label5.setTypeface(astromonkey_font);
        confirm.setTypeface(astromonkey_font);
        back.setTypeface(astromonkey_font);

        final StashPlan plan = (StashPlan) getIntent().getSerializableExtra("StashPlan");
        Item item = plan.getItem();
        name.setText(item.getName());
        price.setText(item.getNicePrice());

        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Item_Save_Activity.this, New_Plan_Spend_Activity.class);
                startActivity(otherIntent);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(Item_Save_Activity.this, New_Plan_Duration_Activity.class);
                otherIntent.putExtra("StashPlan", plan);
                startActivity(otherIntent);
            }
        });
    }
    }
