package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.deepak.stash1.Stash.StashFlow;
import com.example.deepak.stash1.Stash.StashPlan;

import java.util.Date;

public class New_Plan_Duration_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan_duration);

        //just for fonts
        TextView label1 = (TextView) findViewById(R.id.label1);
        TextView label2 = (TextView) findViewById(R.id.label2);
        TextView label3 = (TextView) findViewById(R.id.label3);
        TextView label4 = (TextView) findViewById(R.id.label4);

        TextView continue_butt = (TextView) findViewById(R.id.continue_button);
        TextView back = (TextView) findViewById(R.id.back_button_item_save);

        final EditText days = (EditText) findViewById(R.id.num_of_days);
        final EditText cash = (EditText) findViewById(R.id.cash_flow);
        final EditText stash = (EditText) findViewById(R.id.stash_flow);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");

        label1.setTypeface(astromonkey_font);
        label2.setTypeface(astromonkey_font);
        label3.setTypeface(astromonkey_font);
        label4.setTypeface(astromonkey_font);
        continue_butt.setTypeface(astromonkey_font);
        back.setTypeface(astromonkey_font);

        final Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.duration_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

       final Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.cash_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);

        final TextView error_days = (TextView) findViewById(R.id.error_days);
        final TextView error_cash = (TextView) findViewById(R.id.error_cash);
        final TextView error_percent = (TextView) findViewById(R.id.error_percent);

        final StashPlan plan = (StashPlan) getIntent().getSerializableExtra("StashPlan");
        final Date today = new Date();
        plan.setCreatedOn(today);


        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(New_Plan_Duration_Activity.this, Item_Save_Activity.class);
                otherIntent.putExtra("StashPlan", plan);
                startActivity(otherIntent);
            }
        });

        continue_butt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean flag = false;
                String days1 = days.getText().toString();
                if (days1.equals("") || Integer.parseInt(days1.toString()) <= 0) {
                    flag = false;
                    error_days.setText("Invalid entry");
                }
                else {
                    flag = true;
                    error_days.setText("");
                }

                String cash1 = cash.getText().toString();
                if (cash1.equals("") || Double.parseDouble(cash1) <= 0) {
                    flag = false;
                    error_cash.setText("Invalid entry");
                }
                else {
                    flag = true;
                    error_cash.setText("");
                }

                String stash1 = stash.getText().toString();
                if (stash1.equals("") || Double.parseDouble(stash1) <= 0 || Double.parseDouble(stash1) > 100 ) {
                    flag = false;
                    error_days.setText("Invalid entry");
                }
                else {
                    flag = true;
                    error_days.setText("");
                }

                if (flag) {
                    Date end_date = new Date();
                    int days_num = Integer.parseInt(days.getText().toString());

                    if (String.valueOf(spinner1.getSelectedItem()).equals("Days")) {
                        end_date.setDate(today.getDay() + days_num);
                    }
                    else if (String.valueOf(spinner1.getSelectedItem()).equals("Weeks")) {
                        end_date.setDate(today.getDate() + 7*days_num);
                    }
                    else {
                        end_date.setMonth(today.getMonth() + days_num);
                    }
                    plan.setEndDate(end_date);

                    StashFlow flow = new StashFlow();

                }

                Intent otherIntent = new Intent(New_Plan_Duration_Activity.this, Item_Save_Activity.class);
                otherIntent.putExtra("StashPlan", plan);
                startActivity(otherIntent);
            }
        });
    }
}
