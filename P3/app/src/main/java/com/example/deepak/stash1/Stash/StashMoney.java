package com.example.deepak.stash1.Stash;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.eBay.Finding.FindItemsAdvanced;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
//import java.util.Scanner;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//
//public class StashMoney
//{
//	public static Scanner in = new Scanner(System.in);
//	public static StashPlan s = new StashPlan();
//	public static Item myItem = new Item();
//	public static ArrayList<Item> myItems = new ArrayList();
//
//	public static String acctName = "cs275-kfl29";
//	public static String appKeyName = "myFirstApp";
//	public static String appKeyValue = "JrHwaWpa4l7hUa0eZLGRZdHwEOEYDwPY";
//
//	public static String[] services = {"ebay", "No, but thanks!"};
//
//	public static String APPID = "DrexelUn-StashMon-SBX-e38c4f481-6c9b330a";
//
//	public static FindItemsAdvanced.FindItemsAdvancedResultSet TembooEbaySearch(TembooSession ses, String id, String res, String words, int entries, boolean sand) throws TembooException, TembooException {
//		FindItemsAdvanced findItemsAdvancedChoreo = new FindItemsAdvanced(ses);
//
//		// Get an InputSet object for the choreo
//		FindItemsAdvanced.FindItemsAdvancedInputSet findItemsAdvancedInputs = findItemsAdvancedChoreo.newInputSet();
//
//		// Set inputs
//		findItemsAdvancedInputs.set_ResponseFormat(res);
//		findItemsAdvancedInputs.set_Keywords(words);
//		findItemsAdvancedInputs.set_AppID(id);
//		findItemsAdvancedInputs.set_EntriesPerPage(entries);
//		findItemsAdvancedInputs.set_SandboxMode(sand);
//
//		FindItemsAdvanced.FindItemsAdvancedResultSet getItemsAdvancedResultSet = findItemsAdvancedChoreo.execute(findItemsAdvancedInputs);
//
//		return getItemsAdvancedResultSet;
//
//	}
//
//public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TembooException
//{
//	System.out.println("Welcome to StashPlan Money! \nWould you like to Spend or Save?");
//	s.setStashType(in.nextLine());
//
//	//USER IS SPENDING
//	if(s.getStashType().toLowerCase().equals("spend"))
//	{
//		System.out.println("What are you spending on?");
//		myItem.setName(in.nextLine());
//		System.out.println("Would you like to use one of the following services to search for your item?");
//		for(int i = 0; i < services.length; i++)
//		{
//			System.out.println(services[i]);
//		}
//		myItem.setType(in.nextLine());
//
//		//USER WILL USE EBAY REST SERVICE
//		if(myItem.getType().toLowerCase().trim().equals(services[0].toLowerCase().trim()))
//		{
//			TembooSession sess = new TembooSession(acctName, appKeyName, appKeyValue);
//			FindItemsAdvanced.FindItemsAdvancedResultSet ebayItems = TembooEbaySearch(sess, APPID, "json", myItem.getName(), 3, true);
//
//			System.out.println(ebayItems.get_Response());
//
//			// Now parse the json
//	    	JsonParser jp = new JsonParser();
//	    	JsonElement root = jp.parse(ebayItems.get_Response());
//	    	JsonObject rootobj = root.getAsJsonObject().get("searchResult").getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
//
//	    	JsonArray items = rootobj.get("item").getAsJsonArray();
//	    	for(int i = 0; i < items.size(); i++)
//	    	{
//	    		JsonObject item = items.get(i).getAsJsonObject();
//	    		String title = item.get("title").getAsString();
//	    		System.out.println("Item title " + i + ": " + title);
//	    		myItem.setName(title);
//
//	    		BigDecimal price = item.get("sellingStatus").getAsJsonObject().get("convertedCurrentPrice").getAsJsonObject().get("#text").getAsBigDecimal();
//	    		System.out.println("Item price " + i + ": " + price);
//	    		myItem.setStashAmount(price);
//
//	    		String URL = item.get("viewItemURL").getAsString();
//	    		System.out.println("Item URL " + i + ": " + URL);
//	    		myItem.setUrl(URL);
//
//
//
//	    		myItems.add(myItem);
//	    	}
//	    	s.setStashGoal("Ebay");
//	    	s.setItem(myItems.get(0));
//	    	s.setStashAmount(s.getItem().getStashAmount());
//
////	    	System.out.println("When should you reach your goal by (yyyy-mm-dd):");
////	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
////	    	LocalDate date = LocalDate.parse(in.nextLine(), formatter);
////	    	System.out.println(date);
////	    	s.setEndDate(date);
////
////	    	LocalDate today = LocalDate.parse(LocalDate.now().toString(), formatter);
////	    	s.setCreatedOn(today);
////
////	    	int year = s.getEndDate().getYear() - s.getCreatedOn().getYear();
////	    	int month = ((12 * year) + s.getEndDate().getMonthValue()) - s.getCreatedOn().getMonthValue();
////	    	int day = (int) (s.getEndDate().toEpochDay() - s.getCreatedOn().toEpochDay());
////
////	    	System.out.println("Let us know how much cash is coming in per week(Amount [25.00]:");
////
////	    	System.out.println("Amount...");
////	    	s.setCflow(in.nextBigDecimal());
////	    	s.setSflow(new StashFlow());
////	    	s.getSflow().setCflow(s.getCflow());
////
////	    	System.out.println("What percent of this do you want to put to your stash?:");
////	    	s.getSflow().setPercent(new BigDecimal(in.nextInt()));
////	    	s.calculateStashPlan(year, month, day);
//	    	}
//	}
//	}
//}