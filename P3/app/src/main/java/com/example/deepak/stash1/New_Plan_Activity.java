package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/** Created by Kate Arskaya on 03.01.2016 */

public class New_Plan_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan);

        TextView question = (TextView) findViewById(R.id.question_text);
        Button save_button = (Button) findViewById(R.id.save_plan_button);
        Button spend_button = (Button) findViewById(R.id.spend_plan_button);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");
        question.setTypeface(astromonkey_font);
        save_button.setTypeface(astromonkey_font);
        spend_button.setTypeface(astromonkey_font);

        ImageButton x_close = (ImageButton) findViewById(R.id.close);

        x_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(New_Plan_Activity.this, Welcome_Screen_Activity.class);
                startActivity(otherIntent);
            }
        });

        spend_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(New_Plan_Activity.this, New_Plan_Spend_Activity.class);
                startActivity(otherIntent);
            }
        });

    }


}
