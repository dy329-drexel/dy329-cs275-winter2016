package com.example.deepak.stash1.Stash;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class StashPlan implements Serializable
{
	private Date createdOn;
	private Date endDate;
	private BigDecimal stashAmount;
	private BigDecimal cflow;
	private StashFlow sflow;
	private String stashType;
	private String stashGoal;
	private Item item;
	private int days;

	@Override
	public StashPlan clone() throws CloneNotSupportedException
	{
		StashPlan stashPlan = (StashPlan)super.clone();
		stashPlan.item = (Item)item.clone();
		stashPlan.sflow = (StashFlow)sflow.clone();
		return stashPlan;
	}
		
	public Date getCreatedOn() 
	{
		return createdOn;
	}
	
	public void setCreatedOn(Date createdOn) 
	{
		this.createdOn = createdOn;
	}
	
	public Date getEndDate() 
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate) 
	{
		this.endDate = endDate;
	}

	public BigDecimal getStashAmount() 
	{
		return stashAmount;
	}

	public void setStashAmount(BigDecimal stashAmount) 
	{
		this.stashAmount = stashAmount;
	}

	public StashFlow getSflow() 
	{
		return sflow;
	}

	public void setSflow(StashFlow sflow) 
	{
		this.sflow = sflow;
	}

	public String getStashType() 
	{
		return stashType;
	}

	public void setStashType(String stashType) 
	{
		this.stashType = stashType;
	}

	public String getStashGoal() 
	{
		return stashGoal;
	}

	public void setStashGoal(String stashGoal) 
	{
		this.stashGoal = stashGoal;
	}

	public Item getItem() 
	{
		return item;
	}

	public void setItem(Item item) 
	{
		this.item = item;
	}

	public void setTime(int year, int month, int day)
	{

	}

	public int getDays()
	{
		return days;
	}

	public void setDays(int days)
	{
		this.days = days;
	}

	public StashPlan[]  makeGoodPlan(BigDecimal orig, BigDecimal plan) throws CloneNotSupportedException
	{
		if(this.calculateStashPlan() == true)
			return null;

		StashPlan[] improves = new StashPlan[2];
		StashPlan s = new StashPlan();
		StashPlan s2 = new StashPlan();
		s = clone();
		s2 = clone();

		BigDecimal x = orig.divide(plan);
		BigDecimal perc = s.getSflow().getPercent().multiply(x);
		System.out.println("Old Percent: " + this.getSflow().getPercent());
		s.getSflow().setPercent(perc);
		System.out.println("New Percent: " + s.getSflow().getPercent());

		BigDecimal y = new BigDecimal(1).divide(x, 2, RoundingMode.HALF_UP);
		int days = s2.getDays()* Integer.valueOf(y.ROUND_CEILING);
		System.out.println("Old days: " + this.getDays());
		s2.setDays(days);
		System.out.println("New days: " + s2.getDays());

		improves[0] = s;
		improves[1] = s2;
		return improves;

	}

	public boolean calculateStashPlan()
	{
		BigDecimal[] plans = setPlans();

		if(plans[0].compareTo(plans[1]) > 0)
		{
			System.out.println("Your plan won't work. You must be making at least $" + plans[0] + " a week to reach your goal!!! \n" );
			System.out.println("You are making $" + cflow + " a week, currently \n");
			System.out.println("You are putting away $" + plans[1] + " a week, currently \n");

			return false;
		}
		System.out.println("CONGRATS!!! This plan is a keeper! You must be making at least $" + plans[0] + " a week to reach your goal \n");
		System.out.println("You are making $" + cflow + " a week, currently \n");
		System.out.println("You are putting away $" + plans[1] + " a week, currently \n");
		return true;
	}

	public BigDecimal[] setPlans()
	{
		int weekD = this.getDays() / 7;

		System.out.println("Stash Amount: " + this.getStashAmount());

		System.out.println("weekD: " + weekD);

		BigDecimal[] newPlans = new BigDecimal[2];

		BigDecimal s = this.getStashAmount().divide(new BigDecimal(weekD), 2, RoundingMode.HALF_UP);
		s.setScale(2, RoundingMode.CEILING);
		System.out.println("Money/week: $" + s);

		BigDecimal plan = sflow.calculateStashFlow(cflow, sflow.getPercent());
		plan.setScale(2, RoundingMode.CEILING);

		newPlans[0] = s; //minimum plan
		newPlans[1] = plan; //user plan

		return newPlans;
	}
}
