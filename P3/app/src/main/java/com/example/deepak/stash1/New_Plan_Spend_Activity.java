package com.example.deepak.stash1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.deepak.stash1.Stash.Item;
import com.example.deepak.stash1.Stash.StashPlan;

import java.math.BigDecimal;


/**
 * Created by Kate Arskaya on 3/4/16.
 */
public class New_Plan_Spend_Activity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan_spend);


        TextView tv1 = (TextView) findViewById(R.id.header1);
        TextView tv2 = (TextView) findViewById(R.id.header2);
        TextView tv3 = (TextView) findViewById(R.id.or_textView);

        final EditText item_name = (EditText) findViewById(R.id.item_name_manually);
        final EditText item_price = (EditText) findViewById(R.id.item_price_manually);

        Button add = (Button) findViewById(R.id.add_item_manually);
        Button ebay = (Button) findViewById(R.id.choose_ebay_button);
        TextView back = (TextView) findViewById(R.id.back_arrow);

        final TextView error_name = (TextView) findViewById(R.id.error_name);
        final TextView error_price = (TextView) findViewById(R.id.price_error);

        Typeface astromonkey_font = Typeface.createFromAsset(getAssets(),  "fonts/DKAstromonkey.otf");

        tv1.setTypeface(astromonkey_font);
        tv2.setTypeface(astromonkey_font);
        tv3.setTypeface(astromonkey_font);
        add.setTypeface(astromonkey_font);
        ebay.setTypeface(astromonkey_font);

        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(New_Plan_Spend_Activity.this, New_Plan_Activity.class);
                startActivity(otherIntent);
            }
        });

        ebay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent otherIntent = new Intent(New_Plan_Spend_Activity.this, Ebay_Activity.class);
                startActivity(otherIntent);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean flag = false;
                double price = 0;
                BigDecimal bdPrice = null;

                if (item_name.getText().toString().equals("")) {
                    error_name.setText("Invalid item name");
                    flag = false;
                }
                else {
                    error_name.setText("");
                    flag = true;
                }

                if (item_price.getText().toString().equals("")) {
                    error_price.setText("Invalid price");
                    flag = false;
                }
                else {
                    error_price.setText("");
                    flag = true;
                }

                try {
                    price = Double.parseDouble(item_price.getText().toString());
                } catch (NumberFormatException e) {
                    flag = false;
                }

                if (flag) {
                    StashPlan plan = new StashPlan();
                    Item item = new Item();
                    item.setName(item_name.getText().toString());

                    bdPrice = new java.math.BigDecimal(String.valueOf(price));
                    item.setStashAmount(bdPrice);

                    plan.setItem(item);

                    //http://stackoverflow.com/questions/2736389/how-to-pass-an-object-from-one-activity-to-another-on-android
                    Intent otherIntent = new Intent(New_Plan_Spend_Activity.this, Item_Save_Activity.class);
                    otherIntent.putExtra("StashPlan", plan);
                    startActivity(otherIntent);
                }
            }
        });



    }



}
