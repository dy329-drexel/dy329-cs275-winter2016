Author : Deepak Yadav  		User ID:dy329
	      
In this assignment we need to move dropbox file specified in /move/__list.txt file to our local directory also specified in this program.
I used temboo library(choreos) for this program.
First I initialized Oauth process by my app key and asked user to go to authorization url and giver permisions to us.
After that we finalize Oauth and got our access and secret token .
Using these we accessed out __list.txt file and got our file names and folder names where we want to copy those files.
Using for loop we iterated each line from __list.txt file and parse filename and path from line.
and Using Temboo search files and folder choreos we checked that file on dropbox and parsed its path on dropbox.
Then We decode and wrote our file to local directory using output buffered writer.
After writing file we just upload another empty __list file on drop box and rewrrite our old file so that when we run next time it wont copy again.

This program need a __list.txt file in move folder in root of dropbox and the text file should contain file name and local destination
seperated by space and next file name should be in new line.
If  we do not have this file or the data in file is in wrong format it will throw exception accordingly.


A screencast video is also saved in A1 folder.