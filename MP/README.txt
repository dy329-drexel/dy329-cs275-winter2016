Author:Deepak Yadav  		User ID:dy329
Mid term practicum

In this assignment we used temboo library to connect to twitter timeline and than collecting 30 tweets from a user's timeine.
After that we find polysyllablic words and count and store them and calculate SMOg grade for that user
which is 
grade = 1.0430 * sqrt(number_of_polysyllables * (30 / number_of_sentences)) + 3.1291 
where number_of_polysyllables is the count of words with three or more syllables. 

We used temboo Oauth choeroes for oauth process.
In the begging we used oauth to get permission for user's twitter account by sending him to authorization url
and when we got our access and secret token we used temboo choero usertimeline to get 30 tweets from that user.

Parsing json response from the above request we spilited each tweet into words .
we remove # and @ from each word and if the word have more than 4 letters we will use wordnik api to  check for syllables 
and if the word have 3 or more syllables we will store that word in polysyllable array.When we checked all words from 30 tweets we will
calculate SMOG grade and display grade as well as all polysyllable words and their count.

Sample cases output: user:taylorswift13
Polysyllabic word count in user's 30 tweets: 12
List of polysyllabic words 
positivity
reminder
amazing,
channeling
anyone
amazing
encouraging
together.
performance
favorite
amazing!!
remember
SMOG grade level for user: 6.742157984588678

A screencast is video file also added in directory.This program will take more time to run so it the video is slightly long (6-7 mins) 
Other test case. User:Bill gates
Polysyllabic word count in user's 30 tweets: 38
List of polysyllabic words 
every
recently
tinkering
energy
area
historically
economic
american
volunteers
astonishing:
emergency
education:
digital
technology
amazing
extraordinary
commitment
computer
essential
anyone
introduction
celebrating
commitment
malaria
epidemic.
revolution
every
innovation
accelerating
digital
technology
fundamentally
reorganizing
castaway
decided
century:
consider
parasitic
SMOG grade level for user: 9.558583805096642

