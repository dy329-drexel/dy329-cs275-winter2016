import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooSession;

public class midtermPracticum {
	
	public static void main(String args[]) throws Exception {
		
		//twitter application keys 
	 ArrayList<String> polyWords=new ArrayList<String>();  //array list to store polysyllables 
	String key="POQ3uVvzL7UWzKaTlcX7REpCT";
	String secretkey="rpJg5uiEYTNrgkYpEiZLQuPUpVpe7qMXha4JFcK2NdQ44w9FIk";
	
	
	TembooSession session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");

	InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

	// Get an InputSet object for the choreo
	InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

	// Set inputs
	initializeOAuthInputs.set_ConsumerKey(key);
	initializeOAuthInputs.set_ConsumerSecret(secretkey);

	// Execute Choreo
	InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
	
	String authURL=initializeOAuthResults.get_AuthorizationURL();
	//waiting for user to authorize
	
	System.out.println("Please go to this url and authorize permissions:-  "+authURL);
	System.out.println("type any key and enter when completed authorization");
	Scanner in=new Scanner(System.in);
	String input=in.nextLine();
	
	
	String callbackID=initializeOAuthResults.get_CallbackID();
	String secrettoken=initializeOAuthResults.get_OAuthTokenSecret();
	
FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

	// Get an InputSet object for the choreo
	FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

	// Set inputs
	finalizeOAuthInputs.set_OAuthTokenSecret(secrettoken);
	finalizeOAuthInputs.set_ConsumerKey(key);
	finalizeOAuthInputs.set_CallbackID(callbackID);
	finalizeOAuthInputs.set_ConsumerSecret(secretkey);

	// Execute Choreo
	FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
	String accessToken=finalizeOAuthResults.get_AccessToken();
	String tokenSecret=finalizeOAuthResults.get_AccessTokenSecret();
	
	UserTimeline userTimelineChoreo = new UserTimeline(session);

	// Get an InputSet object for the choreo
	UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();
   System.out.println(accessToken);
   System.out.println(secrettoken);
	// Set inputs
	userTimelineInputs.set_ConsumerKey(key);
	userTimelineInputs.set_AccessToken(accessToken);
	userTimelineInputs.set_ScreenName("taylorswift13");
	userTimelineInputs.set_ConsumerSecret(secretkey);
	userTimelineInputs.set_AccessTokenSecret(tokenSecret);
	userTimelineInputs.set_Count("30");
	// Execute Choreo
	UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);

	
	 JsonParser jp = new JsonParser();
	JsonElement rootobj=jp.parse(userTimelineResults.get_Response());
     JsonArray timeLine=rootobj.getAsJsonArray();
  

     int count=0;
		for(int i=0; i<timeLine.size(); i++) {
         
         //getting a tweet and spilitting into words
			
         JsonObject tweets = timeLine.get(i).getAsJsonObject();
         String txt = tweets.get("text").getAsString();
         String[] words = txt.split(" ");
       
         //checking for each word for polysyllables 
        for(int j=0; j<words.length; j++){
        	
             //Remove any hashtag or @ from words
             String word = words[j].toLowerCase().replace("#", "");
           word = words[j].toLowerCase().replace("@", "");
            
             //next iteration if word is less than 4 characters
            if(word.length()>=4){
            	//System.out.println(word);
             //Checking for syllables in word using wordnik
            	try{
		String URL = "http://api.wordnik.com/v4/word.json/" + word + "/hyphenation?useCanonical=true&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"; 
             URL wordnikURL = new URL(URL);
             HttpURLConnection request = (HttpURLConnection) wordnikURL.openConnection();
             request.connect();
             
          
              
                 JsonElement root = jp.parse(new InputStreamReader((InputStream)request.getContent()));
                 JsonArray syllables = root.getAsJsonArray();
                 
                 //getting number of last syllable in word
                 JsonObject last = (syllables.get(syllables.size()-1)).getAsJsonObject();
                 
                 //checking that word have 3 or more syllables
                 int seqCount = last.get("seq").getAsInt();
                 if(seqCount >= 2) {
                    //adding the word and incresing count
                     polyWords.add(word);
                    
                 }
            }
            catch(Exception e)
            {
            	
            }}
            else 
            	continue;
         
        }}
     //Printing word polysyllabic word count and words
   count=polyWords.size();
     System.out.println("Polysyllabic word count in user's 30 tweets: "+ count);
     System.out.println("List of polysyllabic words ");
     for(int i=0;i<polyWords.size();i++) {
         System.out.println(polyWords.get(i));
        
     }
     
     //Calculates smog grade and print iit on screen
     double smogGrade = 1.0430 * Math.sqrt(polyWords.size())+3.1291;
     System.out.println("SMOG grade level for user: "+smogGrade);
 }
	
	}
	
	
	
	

