Participants : Deepak Yadav  		User ID:dy329

In this assignment I used android studio to create an application FriendNearby(Who's around). I used temboo library to connect to cloudmine and string information.
I created my login page so that user have to login first to view friends which are near. after entering email and password my app will authenticate user with cloudmine server and 
show message fail if login failed and if login successful then it will go to next page.

My next page will have two button first is to access my location and find friends which are near less then 50kms to our location and and their location info is not older than one day i.e 24 hour.

I used Async task to connect to cloudmine using temboo choreo and then I downloaded all objects information and parse it. I am also passing my location coordinates to this async task .

after parsing response from temboo I am calculating distance between my coordinates and object's(other users) coordinates and using matmatical calculation(great cicrle distance) to calculate distance 
between them. I am also comparing system's date and time and objects date and time . If the distance is less and 50kms and the object is not more than one day old than i will store 
it in arraylist of my person class variable.I attached a custom array adapter to my scrolling listview and displayed all the result who are not more than 50kms away from my
location and updated within 1 day.

I displayed list with email, their latitude and longitude and distance from our location.
I also addded onitemlongClicklistner for my list and if user long press on the list item it will invoke an email intent with added email address of that user and we can send an email
to that user which we selected by long pressing on list.

Sadly sending email was not working properly on my emulator so I used my android phone to test it and it was working perfectly on my phone.

Adding pointers on map(Extra credit accoring to professor Bill Mongan):
I also added button on my second screen that has text show on map and I attached that button to my google map activity.
I created an google maps api key adn used that in my map activity.
I passed all the coordinates from my ArrayList<person> using 2 parallel arrays and on my map activites I showed all those peoples location using marker.


I also added screenshots from my phone to this folder for email and map part. all other working is in screencast in folder A2.

 data which i uploaded to cloumine for my project:-
also add libraries in my libs folder to project dependecies on your computer because i have my localaddress.
login details to test: email:deepak8694@gmail.com
			Password:qwerty


i uploaded objects to cloumine with latitude and longitude external to my experiment for my assignment how ever if I hadmultiple people uploading to same application i
wouldt have needed it

{
"users":[

{"email":"dy329@drexel.edu","lat":39.951365 ,"long":-75.157936,"date":"2016-03-15 00:01:38"},
{"email":"kranthi.teja8@gmail.com","lat":39.949692,"long":-75.164285,"date":"2016-03-16 00:01:38"},

{"email":"sample1@drexel.edu","lat":40.015638,"long":-75.746992,"date":"2016-03-15 00:01:38"},
{"email":"sample2@drexel.edu","lat":39.947719,"long":-75.685208,"date":"2016-03-15 00:01:38"},

{"email":"sample3@drexel.edu","lat":40.026944 ,"long":-75.118304,"date":"2016-03-15 00:01:38"},
{"email":"sample4@drexel.edu","lat":39.921355,"long":-74.789602,"date":"2016-03-15 00:01:38"},

{"email":"sample5@drexel.edu","lat":41.82453 ,"long":-70.884086,"date":"2016-03-15 00:01:38"},
{"email":"sample6@drexel.edu","lat":39.928691,"long":-74.989981,"date":"2016-03-15 00:01:38"},

{"email":"sample7@drexel.edu","lat":39.968098 ,"long":-75.246085,"date":"2016-03-12 00:01:38"}


]
}