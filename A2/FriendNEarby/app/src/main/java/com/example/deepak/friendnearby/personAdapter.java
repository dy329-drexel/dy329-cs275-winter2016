package com.example.deepak.friendnearby;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Deepak on 3/14/2016.
 */
public class personAdapter extends ArrayAdapter<person>{


    public personAdapter(Context context, ArrayList<person> details) {
        super(context, 0, details);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

        person det1 = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
        }
        // Lookup view for data population
        TextView email = (TextView) convertView.findViewById(R.id.emailView);
        TextView lati = (TextView) convertView.findViewById(R.id.latView);
        TextView longi = (TextView) convertView.findViewById(R.id.longView);
        TextView dist = (TextView) convertView.findViewById(R.id.distView);








        //   Populate the data into the template view using the data object
        email.setText(det1.email);
        lati.setText(""+det1.lat);
        longi.setText(""+det1.longi);
        dist.setText(""+det1.dis);
        // abc.setImageBitmap(det1.bmp);
        // Return the completed view to render on screen
        return convertView;
    }
}


