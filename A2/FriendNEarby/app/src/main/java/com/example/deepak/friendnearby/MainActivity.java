package com.example.deepak.friendnearby;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class MainActivity extends AppCompatActivity {
 String session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        Button signin_button=(Button)findViewById(R.id.sign_in_button);
        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText email = (EditText) findViewById(R.id.email_box_signin);

                final EditText pw1 = (EditText) findViewById(R.id.pwd_box_signin);
                final String uid = email.getText().toString();
                final String pass = pw1.getText().toString();



                Login auth = new Login();
                   auth.execute(uid, pass);
                    //  String  temp1=auth.getSessiont();
              //  Toast.makeText(MainActivity.this, "session is"+temp1, Toast.LENGTH_SHORT).show();
               //
             //       }
             //   else{

              //      }


            }
        });




    }
    protected void startdisplay(String a){
        String sess=a;

        if(sess=="") {
            Toast.makeText(MainActivity.this,"failed" , Toast.LENGTH_SHORT).show();
        }
            else {
            Log.d("stoken",sess);
            Intent otherIntent = new Intent(MainActivity.this, FindFriends.class);
            otherIntent.putExtra("session_token", sess);
            startActivity(otherIntent);
        }
    }


    public class Login extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... params) {
            String email=params[0];
            String pass=params[1];
            String abc="";
            TembooSession session = null;
            try {
                session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");
            } catch (TembooException e) {
                e.printStackTrace();
            }

            AccountLogin accountLoginChoreo = new AccountLogin(session);

// Get an InputSet object for the choreo
            AccountLogin.AccountLoginInputSet accountLoginInputs = accountLoginChoreo.newInputSet();

// Set inputs
            accountLoginInputs.set_APIKey("f427c4f5ea974ddca7f2b0e85ac23849");
            accountLoginInputs.set_Username(email);
            accountLoginInputs.set_ApplicationIdentifier("bd91f469d02b9c4890b95050a117d588");
            accountLoginInputs.set_Password(pass);

// Execute Choreo
            try {
                AccountLogin.AccountLoginResultSet accountLoginResults = accountLoginChoreo.execute(accountLoginInputs);
                //    sessiont= accountLoginResults.getResultString("session_token");

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(accountLoginResults.get_Response());
                JsonObject rootobj=root.getAsJsonObject();

                abc=rootobj.get("session_token").getAsString();


            } catch (TembooException e) {
                e.printStackTrace();
            }

            return abc;
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            startdisplay(result);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
