package com.example.deepak.friendnearby;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet.ObjectGetInputSet;
import com.temboo.Library.CloudMine.UserAccountManagement.GetUsers;
import com.temboo.Library.CloudMine.UserAccountManagement.GetUsers.GetUsersInputSet;
import com.temboo.Library.CloudMine.UserAccountManagement.GetUsers.GetUsersResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Deepak on 3/14/2016.
 */
public class FindUser extends AsyncTask<Void,Void,Void> {

    ArrayList<person> data1;
    personAdapter adapter;
    double plati;
   double plong;

public FindUser(ArrayList<person> data2, personAdapter adap,double plati1,double plong1) {
    data1=data2;
    adapter=adap;
    plati=plati1;
    plong=plong1;

}
    public person getselecteditem(int pos){
        return data1.get(pos);
    }
    public ArrayList<person> getalldata(){
        return data1;
    }
    @Override
    protected Void doInBackground(Void... params) {


        TembooSession session = null;
        try {
            session = new TembooSession("deepak8694", "myFirstApp", "rNhwDwaFqAbHQkmwDiz4m4or8rPznqzu");
        } catch (TembooException e) {
            e.printStackTrace();
        }

        ObjectGet objectGetChoreo = new ObjectGet(session);

// Get an InputSet object for the choreo
        ObjectGetInputSet objectGetInputs = objectGetChoreo.newInputSet();

// Set inputs
        objectGetInputs.set_APIKey("f427c4f5ea974ddca7f2b0e85ac23849");
     //   objectGetInputs.set_SessionToken("6a87970ab3384406b741185ace5f181f");
        objectGetInputs.set_ApplicationIdentifier("bd91f469d02b9c4890b95050a117d588");

// Execute Choreo
        try {
            ObjectGet.ObjectGetResultSet objectGetResults = objectGetChoreo.execute(objectGetInputs);


            JsonParser gson1 = new JsonParser();
            JsonElement root1 = gson1.parse(objectGetResults.get_Response());
            JsonObject rootobj1 = root1.getAsJsonObject();
            JsonObject rootobj2 = rootobj1.get("success").getAsJsonObject();

            JsonArray ar = rootobj2.get("users").getAsJsonArray();

            for (int i = 0; i < ar.size(); i++) {
                JsonObject data = ar.get(i).getAsJsonObject();
                String email = data.get("email").getAsString();
                String oldtime = data.get("date").getAsString();
                double lati = data.get("lat").getAsDouble();
                double longi = data.get("long").getAsDouble();
              //  Log.d("longitudes", "" + plati);
                double ct = Math.PI / 180;


                double theta = plong - longi;
                double dist = Math.sin(ct * plati) * Math.sin(ct * lati) + Math.cos(ct * plati) * Math.cos(ct * lati) * Math.cos(ct * theta);
                dist = Math.acos(dist);
                dist = dist * (180 / Math.PI);
                dist = dist * 60 * 1.1515;
                double distanceinkm = dist * 1.609344;
                distanceinkm = Math.floor(distanceinkm * 100) / 100;
                Date dt = new Date();
                Log.d("date", "" + dt);

                Date curdate = new Date();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date oldDate = dateFormat.parse(oldtime);
                    Long timeDiff = curdate.getTime() - oldDate.getTime();
                    int day = (int) TimeUnit.MILLISECONDS.toDays(timeDiff);
                    Log.d("difference",""+day);
                    if ((day <= 1) && (distanceinkm <= 50)) {

                        person abc;
                        abc = new person(email, lati, longi, distanceinkm,oldtime);
                        data1.add(abc);
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }


          //      person abc;
             //  abc = new person(email, lati, longi, distanceinkm);
              //  data1.add(abc);

            }


        }

         catch (TembooException e) {
            e.printStackTrace();
        }


        return null;
    }
    @Override
    protected void onPostExecute(Void param)
    {
        adapter.notifyDataSetChanged();
    }

}
