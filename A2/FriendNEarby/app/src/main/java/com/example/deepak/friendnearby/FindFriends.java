package com.example.deepak.friendnearby;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class FindFriends extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button find = (Button) findViewById(R.id.button);
     //   ListView lv = (ListView) findViewById(R.id.listView);



        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            ArrayList<person> xyz = new ArrayList<person>();

                Location l = getLoc();
                double lala1 = l.getLatitude();
                double longi1 = l.getLongitude();

                personAdapter adapter = new personAdapter(FindFriends.this, xyz);
              final  FindUser task = new FindUser(xyz, adapter, lala1, longi1);
                ListView lst = (ListView) findViewById(R.id.listView);
                lst.setAdapter(adapter);
                task.execute();


                Button b1=(Button)findViewById(R.id.showmapbutton);
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    ArrayList<person> allfriends = new ArrayList<person>();
                        allfriends = task.getalldata();
                        int size=allfriends.size();
                        Log.d("size of array","hehehe"+size);
                        double arr[] = new double[size];
                        double arr1[] = new double[size];
                         try {
                             for (int i = 0; i<size; i++) {
                                 Log.d("I===",""+i);
                                          person p = task.getselecteditem(i);
                                          arr[i] = p.getlat();
                                          arr1[i] = p.getlongi();
                                          Log.d("latitude",""+arr[i]);
                                       }
                         } catch (Exception e) {
                                      Log.d("error","outof bound");
                                   }

                                 Intent otherIntent = new Intent(FindFriends.this, MapsActivity.class);
                                 otherIntent.putExtra("arr", arr);
                                 otherIntent.putExtra("arr1", arr1);

                                 startActivity(otherIntent);

                             }

                });



                lst.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()

                {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                                   long id) {


                        person pselect = task.getselecteditem(position);
                        Toast.makeText(FindFriends.this, "pos" + pselect.getemail(), Toast.LENGTH_SHORT).show();

                        String email = pselect.getemail();
                        String[] TO = {email};
                        String[] CC = {""};
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                        emailIntent.putExtra(Intent.EXTRA_CC, CC);
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                            finish();
                            Log.i("Finished sending ema.", "");
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(FindFriends.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                        }


                        return false;
                    }
                });






            }
        });



    }



    public Location getLoc() {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


           double lala1 = 39.968098;
           double longi1=-75.155744;

            Location loc=new Location("");
            loc.setLatitude(lala1);
            loc.setLongitude(longi1);
            return loc;

        }
        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Log.d("location from gps", "" + ""+locationGPS.getLongitude());
        return locationGPS;
    }

}
